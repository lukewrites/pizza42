export function formatPrice(cents) {
    return (cents / 100).toLocaleString("en-us", {
        style: "currency",
        currency: "USD"
    });
}