import React, { Component } from 'react';
import './Profile.css';

class Profile extends Component {
  componentWillMount() {
    this.setState({ profile: {} });
    const { userProfile, getProfile } = this.props.auth;
    if (!userProfile) {
      getProfile((err, profile) => {
        this.setState({ profile });
      });
    } else {
      this.setState({ profile: userProfile });
    }
  }

  render() {
    return (
      <div className="container">
        <div className="card">
          <img className="card-img-top" src={this.state.profile.picture} alt={this.state.profile.name}/> 
          <div className="card-body">
            <h3 className="card-title">{this.state.profile.name}</h3>
            <div className="card-subtitle text-muted">(My friends call me <span className="name">"{this.state.profile.nickname}"</span>)</div><br />
            <p><strong>Gender: </strong><span className="name">{this.state.profile.gender || `Unknown`}</span></p>
            <p><strong>Google Connections: </strong> {this.state.profile['http://pizza42.com/contactsCount'] || `Unknown`}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;