import React from 'react';
import { render } from 'react-dom';
import MyRouter from './components/Router';
import history from './history';
import App from './App'


render(
    <MyRouter history={history} component={App}/>,
    document.querySelector('#main')
);