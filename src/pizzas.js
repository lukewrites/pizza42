const pizzas = {
    pizza1: {
        name: 'Thanks For All The Fish',
        desc: 'All sorts of seafood, baked onto a pizza.',
        price: 2599,
        image: '/images/pizza1.jpg'
    },
    pizza2: {
        name: 'Mostly Harmless',
        desc: 'If it\'s in our kitchen, we put it on this pie.',
        price: 2499,
        image: '/images/pizza2.jpg'
    },
    pizza3: {
        name: 'The Hitchhiker\'s Special',
        desc: 'Buffalo mozzarella and lean Vogon meat on a thin crust.',
        price: 2499,
        image: '/images/pizza6.jpg'
    },
    pizza4: {
        name: 'Marvin\'s Special',
        desc: 'A plain cheese pizza that is a little soggy. I have the brain the size of a galaxy and I make pizza all day. Am I getting you down?',
        price: 1999,
        image: '/images/marvin-pizza.jpg'
    },
    pizza5: {
        name: 'Deep Thought',
        desc: 'A conceptual pizza cut into 42 pieces. N.B. long prep time.',
        price: 4200,
        image: '/images/pizza4.jpg'
    },
};

export default pizzas;