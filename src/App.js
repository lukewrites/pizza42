import React, { Component } from 'react';
import './App.css';
import Order from './components/Order';
import Menu from './components/Menu';
import pizzas from './pizzas'
import {Link, withRouter} from 'react-router-dom';
import { Route, Redirect } from 'react-router-dom';
import Profile from './Profile/Profile';

class App extends Component {
  state = {
    pizzas: pizzas,
    order: {}
  };

  orderPizza = key => {
    const order = { ...this.state.order };
    order[key] = order[key] + 1 || 1;
    this.setState({ order }); 
  };

  goTo = (route) => {
    this.props.history.replace(`/${route}`)
  };

  login = () => {
    this.props.auth.login();
  };

  logout = () => {
    this.props.auth.logout();
  };

  render() {
    const { isAuthenticated } = this.props.auth;
    return (
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary navbar-static-top">
          <Link className="navbar-brand" to="/">
              Pizza42 <span role="img" aria-label="Pizza, Robot, Thumbs Up (for hitchhiking)">🍕🤖👍</span>
          </Link>
            {
                !isAuthenticated() &&
                <div className="navbar-collapse justify-content-end">
                <button className="btn btn-info" onClick={this.login.bind(this)}>Sign In</button>
                </div>
            }
            {
                isAuthenticated() &&
                <div className="navbar-collapse justify-content-end">
                    <button className="btn btn-info navbar-right" onClick={this.logout.bind(this)}>Sign Out</button>
                </div>
            }
        </nav>
        <div className="container">
          <div className="row">
            <div className="col-sm-3 sidebar fixed">
              <Route path="/" exact render={(props) => <Order pizzas={this.state.pizzas} order={this.state.order} auth={this.props.auth} {...props} />} />
            </div>
            <div className="col-sm-9 menu">
              <Route path="/" exact render={(props) => <Menu auth={this.props.auth} {...props} orderPizza={this.orderPizza} pizzas={this.state.pizzas} />} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
