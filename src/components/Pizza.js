import React from 'react';
import { formatPrice } from '../helpers';

class Pizza extends React.Component {
    handleClick = () => {
        this.props.orderPizza(this.props.index);
    }
    render() {
        const { image, name, desc, price } = this.props.details;
        return (
            <div className="card text-center">
                <img className="card-img-top" src={image} alt={name}/>
                <div className="card-body">
                    <h6 className="card-title">{name}</h6>
                    <p className="card-subtitle mb-2 text-muted">{formatPrice(price)}</p>
                    <p className="card-text">{desc}</p>
                </div>
                <div className="card-footer">
                    <button className="btn btn-success" onClick={this.handleClick}>Add to order</button> 
                </div>
            </div>
        );
    }
}

export default Pizza;
