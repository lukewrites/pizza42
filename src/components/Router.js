import React from "react";
import { Router, Route, Redirect, Switch } from "react-router-dom";
import NotFound from "./NotFound";
import App from "../App";
import Auth from "../Auth/Auth";
import Callback from "./Callback";
import history from "../history";
import Profile from "../Profile/Profile";

const auth = new Auth();

const handleAuthentication = (nextState, replace) => {
  if (/access_token|id_token|error/.test(nextState.location.hash)) {
    auth.handleAuthentication();
  }
};

const MyRouter = () => (
    <Router history={history}>
        <Switch>
            <Route
                path="/"
                exact
                render={props => <App auth={auth} {...props} />}
            />
            <Route
                path="/callback"
                render={props => {
                    handleAuthentication(props);
                    return <Callback {...props} />;
                }}
            />
            <Route component={NotFound} />
        </Switch>
    </Router>
);

export default MyRouter;
