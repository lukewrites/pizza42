import React from 'react';
import {formatPrice} from '../helpers'; 
import Profile from '../Profile/Profile';
import Auth from '../Auth/Auth';

const auth = new Auth();

class Order extends React.Component {
    componentWillMount() {
        const { isAuthenticated } = this.props.auth;
        if (isAuthenticated()) {
            this.setState({ profile: {} });
        const { userProfile, getProfile } = this.props.auth;
            if (!userProfile) {
                getProfile((err, profile) => {
                    this.setState({ profile });
            });
        } else {
            this.setState({ profile: userProfile });
        }
    }
    }

    placeOrder = (canPlaceOrder) => {
        if (canPlaceOrder) {
            alert('Order placed!')
        } else {
            alert('You cannot place an order until you have authenticated your email account.')
        }
    }

    
    renderOrder = key => {
        const pizza = this.props.pizzas[key];
        const count = this.props.order[key];
        return <p key={key}>{count} x {pizza.name} <span className="float-right">{formatPrice(count * pizza.price) }</span></p>
    }

    login = () => {
        auth.login();
    };

    render() {
        const { isAuthenticated } = this.props.auth;
        const orderIds = Object.keys(this.props.order);
        const totals = orderIds.reduce((prevTotal, key) => {
            const pizza = this.props.pizzas[key];
            const count = this.props.order[key];
            return prevTotal + (count * pizza.price);
        }, 0);
        return (
            <div className="order-wrap">
            <h2 className="text-center">Your Order</h2>
            {
                !isAuthenticated() &&
                <button className="btn btn-warning btn-block" onClick={this.login.bind(this)}>Log In To Order</button>
            }
            {
                isAuthenticated() &&
                <div>
                    {orderIds.map(this.renderOrder)}
                    <hr/>
                    {
                        orderIds.length > 0 &&
                        <div>
                        <h6 className="float-right">Total: <strong>{formatPrice(totals)}</strong></h6><br/>
                        {
                            !this.state.profile.email_verified && 
                            <button className="btn btn-warning btn-block" disabled>Verify email to order</button>
                        }
                        {
                            this.state.profile.email_verified &&
                            <button className="btn btn-secondary btn-block" onClick={this.placeOrder}>Place Order!</button>
                        }
                        <br/>
                        </div>
                    }
                    <div>
                      <Profile auth={this.props.auth} />
                    </div>
                </div>
            }
            
            </div>
        )
    }
}

export default Order;