import React from "react";
import { Link, withRouter } from "react-router-dom";
import Auth from "../Auth/Auth";

const auth = new Auth();
class NavBar extends React.Component {
    goTo = route => {
        this.props.history.replace(`/${route}`);
    };

    login = () => {
        auth.login();
    };

    logout = () => {
        auth.logout();
    };
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary navbar-static-top">
                <Link className="navbar-brand" to="/">
                    Pizza42{" "}
                    <span
                        role="img"
                        aria-label="Pizza, Robot, Thumbs Up (for hitchhiking)"
                    >
                        🍕🤖👍
                    </span>
                </Link>
                {!auth.isAuthenticated() && (
                    <div className="navbar-collapse justify-content-end">
                        <button
                            className="btn btn-info"
                            onClick={this.login.bind(this)}
                        >
                            Sign In
                        </button>
                    </div>
                )}
                {auth.isAuthenticated() && (
                    <div className="navbar-collapse justify-content-end">
                        <button
                            className="btn btn-info navbar-right"
                            onClick={this.logout.bind(this)}
                        >
                            Sign Out
                        </button>
                    </div>
                )}
            </nav>
        );
    }
}

export default withRouter(NavBar);
