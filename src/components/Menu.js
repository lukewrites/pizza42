import React from 'react';
import Pizza from './Pizza';
import Profile from '../Profile/Profile';

class Menu extends React.Component {
    render () {
        return (
          <div className="card-columns">
              {Object.keys(this.props.pizzas).map(key => <Pizza key={key} details={this.props.pizzas[key]} index={key}  orderPizza={this.props.orderPizza} auth={this.props.auth} />)}
          </div>
        );
    }
}

export default Menu;