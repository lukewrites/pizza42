# Pizza42: The Best Pizza in the Galaxy
## A serverless single-page app that integrates Auth0 and the Google People API

A simple React app [hosted on netlify](https://pizza42.netlify.com/) that leverages Auth0 and the Google People API (via AWS Lambda) to help you know just _who's_ ordering pizza.

The app has the following features:
- [x] Users sign up/in using Auth0 either using an email/pw or via Google
- [x] The app uses the Rules `Link Accounts with Same Email Address` and `Send email verification message` to do just that.
- [x] The app uses a custom Rule to [add the number of Google Contacts to the user's metadata](https://gitlab.com/lukewrites/pizza42/snippets/1758896)
- [x] The app only allows adding pizzas to cart when a user logged in, however…
- [x] a user can only "order" pizzas if their email is verified
- [x] The app gets user's gender without asking them
- [x] Finally, the app uses the code in [this snippet](https://gitlab.com/lukewrites/pizza42/snippets/1758041) as an AWS Lambda function behind an AWS API endpoint to pull in the number of contacts the user has on google.

The app can be accessed online at https://pizza42.netlify.com or run on local by closing this repo and doing `npm install && npm start` at the command line.

(And I sure hope the `42` in the name of this task was a reference to the Hitchhiker's Guide to the Galaxy, otherwise my pizza shop is going to seem strange.)

## https://pizza42.netlify.com

Screenshots:
============

![not logged in](https://i.imgur.com/R9C6t0C.png)
![logged in, email not verified](https://imgur.com/R3JpmXA.png)
![logged in, email verified](https://imgur.com/5Kjwf39.png)
![rules in use](https://imgur.com/0P6yKnK.png)